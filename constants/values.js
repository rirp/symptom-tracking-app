export const sections = ["Antecedente", "Seguimiento"];

export const documentTypes = [
  {
    label: "Cedula de ciudadanía",
    value: "CC",
  },
  {
    label: "Cedula de extranjería",
    value: "CE",
  },
  {
    label: "Pasaporte",
    value: "PA",
  },
  {
    label: "Permiso especial",
    value: "PE",
  },
];

export const documentTypes2 = [
  {
    label: "Cedula de extranjería",
    value: "CE",
  },
  {
    label: "Pasaporte",
    value: "PA",
  },
  {
    label: "Permiso especial",
    value: "PE",
  },
];

export const gender = [
  {
    label: "Mujer",
    value: "M",
  },
  {
    label: "Hombre",
    value: "H",
  },
];
