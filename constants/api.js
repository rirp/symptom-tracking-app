export const BASE_URL =
  "";

export const LOGIN = BASE_URL + "auth/login";

export const CREATE_EMPLOYEE = BASE_URL + "employee/create";

export const SAVE_ANSWERS = BASE_URL + "userQuestionAnswer/create/list/";

export const FIND_USER_BY_DOCUMENT = BASE_URL + "employee/getByDocument";

export const SAVE_USER_TEMPERATURE = BASE_URL + "userTemperature/create";
