import { makeRequest } from "./Network";
import { SAVE_USER_TEMPERATURE } from "../constants/api";

export const saveTemprature = (employeeId, temprature) => {
  return makeRequest(SAVE_USER_TEMPERATURE, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      employeeId: employeeId,
      temperature: temprature,
    }),
  });
};
