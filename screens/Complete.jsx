import React from "react";
import { Block, Text, Button } from "../components";
import * as Animatable from "react-native-animatable";
import Icon from "react-native-vector-icons/FontAwesome";
import { theme } from "../constants";
import { StackActions, useNavigation } from "@react-navigation/native";
import { useSession } from "../context/context";

export default Complete = () => {
  const navigation = useNavigation();
  const { sessionData } = useSession();

  return (
    <Block bottom color={"white"} padding={[0, theme.sizes.base * 2]}>
      <Block middle center>
        <Animatable.View animation={"slideInDown"}>
          <Icon name="check" color={theme.colors.primary} size={80} />
        </Animatable.View>

        <Animatable.View animation={"slideInUp"}>
          <Text h1 color={theme.colors.secondary}>
            Registro exitoso
          </Text>
        </Animatable.View>
      </Block>
      <Animatable.View
        style={{
          alignSelf: "stretch",
          justifyContent: "flex-end",
          marginBottom: 80,
        }}
        animation={"slideInUp"}
      >
        <Button
          gradient
          onPress={() => {
            navigation.dispatch(StackActions.popToTop());
          }}
        >
          <Text bold white center>
            Finalizar
          </Text>
        </Button>
      </Animatable.View>
    </Block>
  );
};
