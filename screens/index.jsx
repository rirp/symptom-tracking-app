import Welcome from "./Welcome";
import Login from "./Login";
import Home from "./Home";
import SearchUser from "./SearchUser";
import Questionary from "./Questionary";
import Temperature from "./Temperature";
import CreateUser from "./CreateUser";
import Complete from "./Complete";
import WebViewScreen from "./WebViewScreen";

export {
  Welcome,
  Login,
  Home,
  SearchUser,
  Questionary,
  Temperature,
  CreateUser,
  Complete,
  WebViewScreen,
};
