import React, { useEffect, Fragment } from "react";

import {
  Platform,
  SectionList,
  StyleSheet,
  KeyboardAvoidingView,
  ActivityIndicator,
  View,
  TouchableOpacity,
} from "react-native";

import { Block, Text, Card, Button, Input } from "../components";
import QuestionaryStore from "../stores/QuestionaryStore";
import QuestionaryAction from "../actions/QuestionaryAction";
import pickerSelectStyles from "../constants/pickerSelectStyles";
import { theme } from "../constants";
import RNPickerSelect from "react-native-picker-select";
import Icon from "react-native-vector-icons/Entypo";
import { useSession } from "../context/context";
import { SafeAreaView } from "react-native-safe-area-context";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DatePicker from "react-native-datepicker";

const Questionary = ({ route, navigation }) => {
  const {
    handleQuestionary,
    saveAnswerInMemory,
    consumeSaveAnswers,
  } = QuestionaryAction();
  const [questionaryFields, setQuestionaryFields] = QuestionaryStore();
  const { sessionData, showErrorMessage } = useSession();
  const [isDatePickerVisible, setDatePickerVisibility] = React.useState(false);

  useEffect(() => {
    questionaryFields.requestAntecedent = route.params.requestAntecedent;

    questionaryFields.followUp = sessionData.followUp;
    questionaryFields.antecedents = sessionData.antecedents;

    questionaryFields.employeeId = route.params.employeeToSurvey;
  }, []);

  const getSections = () => {
    const ant = questionaryFields.antecedents
      ? questionaryFields.antecedents
      : [];
    const follow = questionaryFields.followUp ? questionaryFields.followUp : [];
    if (route.params.requestAntecedent) {
      return [
        {
          title: "Antecedente",
          data: ant,
        },
        {
          title: "Seguimiento",
          data: follow,
        },
      ];
    }
    return [{ title: "Seguimiento", data: follow }];
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (question, date, isJustification) => {
    hideDatePicker();

    if (isJustification) {
      saveAnswerInMemory(question, {
        answerSelected: question.answerSelected,
        justificationAsnwwer: date,
      });
    } else {
      const value = date.toISOString().split("T")[0];
      saveAnswerInMemory(question, { answerText: value });
    }
  };

  const saveQuestion = () => {
    const answers = handleQuestionary();

    if (!answers || answers.length == 0) {
      showErrorMessage("Debe contestar todas las preguntas");
      setQuestionaryFields({
        loading: false,
      });
    } else {
      consumeSaveAnswers(questionaryFields.employeeId, answers)
        .then((response) => {
          setQuestionaryFields({
            loading: false,
          });
          navigation.navigate("Temperature", {
            employeeToSurvey: questionaryFields.employeeId,
          });
        })
        .catch((error) => {
          showErrorMessage(error.message);
          setQuestionaryFields({
            loading: false,
          });
        });
    }
  };

  const renderOpenOption = (isDate, question) => {
    return isDate === undefined || isDate === false ? (
      <Input
        style={styles.input}
        placeholder=" Escriba su respuesta"
        onChangeText={(text) =>
          saveAnswerInMemory(question, { answerText: text })
        }
      />
    ) : (
      renderDate(question)
    );
  };

  const renderCloseOption = (question) => {
    var answers = [];
    if (question.answers != null && question.answers.length > 0) {
      answers = question.answers.map((a) => ({
        label: a.text,
        value: a.id.toString(),
      }));
    }

    return (
      <View>
        <View style={styles.pickerContainer}>
          <RNPickerSelect
            onValueChange={(value) =>
              saveAnswerInMemory(question, { answerSelected: value })
            }
            items={answers}
            useNativeAndroidPickerStyle={false}
            textInputProps={{ underlineColor: "yellow" }}
            Icon={() => (
              <Icon
                name="chevron-small-down"
                size={24}
                color={theme.colors.primary}
              />
            )}
            style={{
              ...pickerSelectStyles,
              iconContainer: {
                top: 10,
                right: 12,
              },
            }}
          />
        </View>
        {renderJustification(question)}
      </View>
    );
  };

  const renderJustification = (question) => {
    const { justification, answerSelected } = question;

    const text = justification ? justification.text : "-";

    return justification &&
      justification.answerIdToRequest == answerSelected ? (
      <Fragment>
        <Text h3 style={{ marginVertical: 20 }}>
          {text}
        </Text>
        <DatePicker
          style={styles.input}
          date={question.justificationAsnwwer}
          mode="date"
          placeholder="Selecciona una fecha"
          format="DD-MM-YYYY"
          confirmBtnText="Confirmar"
          cancelBtnText="Cancelar"
          showIcon={false}
          customStyles={{
            dateInput: {
              borderRadius: 5,
              paddingLeft: 5,
              borderBottomColor: theme.colors.gray,
              borderWidth: StyleSheet.hairlineWidth,
              alignItems: "flex-start",
              marginBottom: 5,
            },
          }}
          onDateChange={(date) => {
            handleConfirm(question, date, true);
          }}
        />
      </Fragment>
    ) : null;
  };

  const renderDate = (question) => {
    return (
      <View>
        <TouchableOpacity onPress={showDatePicker} style={styles.buttonDate}>
          <View pointerEvents="none">
            <Input
              style={styles.input}
              placeholder=" Seleccione una fecha"
              value={question.answerText}
            />
          </View>
        </TouchableOpacity>
        {isDatePickerVisible ? (
          <DateTimePickerModal
            isVisible={true}
            mode="date"
            onConfirm={(date) => handleConfirm(question, date, false)}
            onCancel={hideDatePicker}
          />
        ) : null}
      </View>
    );
  };

  const renderItem = (question) => {
    return (
      <Card shadow style={{ marginHorizontal: 10 }}>
        <Text h3 style={{ marginBottom: question.open ? 0 : 15 }}>
          {question.question}
        </Text>
        {question.open
          ? renderOpenOption(question.date, question)
          : renderCloseOption(question)}
      </Card>
    );
  };

  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <KeyboardAvoidingView
        style={{ flex: 1, justifyContent: "center" }}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <Block>
          <SectionList
            sections={getSections()}
            keyExtractor={(item, index) => index}
            renderItem={({ item }) => renderItem(item)}
            renderSectionHeader={({ section: { title } }) => (
              <Text semibold h1 style={styles.headerSection}>
                {title}
              </Text>
            )}
            style={{ backgroundColor: "white", padding: theme.sizes.padding }}
          />
          <Button
            gradient
            onPress={() => saveQuestion()}
            style={{ marginHorizontal: theme.sizes.padding - 10 }}
          >
            {questionaryFields.loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text bold white center>
                Guardar cuestionario
              </Text>
            )}
          </Button>
        </Block>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Questionary;

const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
    padding: 20,
    marginVertical: 8,
  },
  header: {
    flex: 0.05,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 10,
  },
  title: {
    fontSize: 24,
  },
  droidSafeArea: {
    backgroundColor: "white",
    flex: 1,
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
  picker: {
    borderColor: theme.colors.black,
    borderRadius: 1,
  },
  headerSection: {
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 10,
    color: theme.colors.primary,
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    marginVertical: 10,
    borderColor: theme.colors.gray2,
    height: 35,
  },
  input: {
    width: "100%",
  },
  pickerContainer: {
    marginTop: 10,
  },
});
