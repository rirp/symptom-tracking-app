import React from "react";
import {
  ScrollView,
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Platform,
  ActivityIndicator,
  Keyboard,
} from "react-native";
import { values, theme } from "../constants";
import pickerSelectStyles from "../constants/pickerSelectStyles";
import { Input, Button, Text, Block } from "../components";
import { SafeAreaView } from "react-native-safe-area-context";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import CreateUserStore, { initialStore } from "../stores/CreateUserStore";
import CreateUserAction from "../actions/CreateUserAction";
import RNPickerSelect from "react-native-picker-select";
import Icon from "react-native-vector-icons/Entypo";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function CreateUser({ navigation }) {
  const [fields, setFields] = CreateUserStore();
  const {
    getJobPositions,
    validateEmployee,
    createEmployee,
  } = CreateUserAction();
  // Input error state
  const { errors } = fields;
  const hasErrors = (key) => (errors.includes(key) ? styles.hasErrors : null);
  const [isDatePickerVisible, setDatePickerVisibility] = React.useState(false);

  React.useEffect(() => {
    setFields(initialStore);
  }, []);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    hideDatePicker();

    setFields({ birthDay: date.toISOString().split("T")[0] });
  };

  const handleCreateUser = () => {
    Keyboard.dismiss();

    validateEmployee();
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "space-between",
        backgroundColor: "white",
      }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <ScrollView>
          <Block padding={[80, theme.sizes.base * 2]}>
            <Text
              h1
              center
              color={theme.colors.primary}
              style={{ marginBottom: 25 }}
            >
              Ingrese los datos del empleado
            </Text>
            <View style={styles.pickerContainer}>
              <RNPickerSelect
                placeholder={values.documentTypes[0]}
                onValueChange={(value, index) =>
                  setFields({ doctType: values.documentTypes2[index] })
                }
                items={values.documentTypes2}
                Icon={() => (
                  <Icon
                    name="chevron-small-down"
                    size={24}
                    color={theme.colors.primary}
                  />
                )}
                useNativeAndroidPickerStyle={false}
                textInputProps={{ underlineColor: "yellow" }}
                style={{
                  ...pickerSelectStyles,
                  iconContainer: {
                    top: 10,
                    right: 12,
                  },
                }}
              />
            </View>
            <Input
              label="Número de documento"
              error={hasErrors("docNumber")}
              style={[styles.input, hasErrors("docNumber")]}
              defaultValue={fields.docNumber}
              onChangeText={(text) => setFields({ docNumber: text })}
            />
            <Input
              label="Primer nombre"
              error={hasErrors("firstName")}
              style={[styles.input, hasErrors("firstName")]}
              defaultValue={fields.firstName}
              onChangeText={(text) => setFields({ firstName: text })}
            />
            <Input
              label="Segundo nombre"
              style={styles.input}
              defaultValue={fields.secondName}
              onChangeText={(text) => setFields({ secondName: text })}
            />
            <Input
              label="Primer apellido"
              error={hasErrors("lastName")}
              style={[styles.input, hasErrors("lastName")]}
              defaultValue={fields.lastName}
              onChangeText={(text) => setFields({ lastName: text })}
            />
            <Input
              label="Segundo apellido"
              style={styles.input}
              defaultValue={fields.surname}
              onChangeText={(text) => setFields({ surname: text })}
            />
            <Input
              label="Dirección de residencia"
              error={hasErrors("address")}
              style={[styles.input, hasErrors("address")]}
              defaultValue={fields.address}
              onChangeText={(text) => setFields({ address: text })}
            />
            <Text gray>Fecha de nacimiento</Text>
            <TouchableOpacity
              onPress={showDatePicker}
              style={styles.buttonDate}
            >
              <Text center gray style={{ flex: 1 }}>
                {fields.birthDay}
              </Text>
              <Icon
                style={{ marginRight: 10 }}
                name="chevron-small-down"
                color={theme.colors.primary}
                size={20}
              />
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
            <Text gray style={{ marginTop: 5 }}>
              Genero
            </Text>
            <View style={[styles.pickerContainer, { marginTop: 10 }]}>
              <RNPickerSelect
                placeholder={values.gender[1]}
                onValueChange={(value) => setFields({ gender: value })}
                items={[{ label: "Mujer", value: "M" }]}
                Icon={() => (
                  <Icon
                    name="chevron-small-down"
                    size={24}
                    color={theme.colors.primary}
                  />
                )}
                useNativeAndroidPickerStyle={false}
                textInputProps={{ underlineColor: "yellow" }}
                style={{
                  ...pickerSelectStyles,
                  iconContainer: {
                    top: 10,
                    right: 12,
                  },
                }}
              />
            </View>
            <Text gray style={{ marginTop: 15 }}>
              Cargo
            </Text>
            <View style={[styles.pickerContainer, { marginTop: 10 }]}>
              <RNPickerSelect
                onValueChange={(value) =>
                  setFields({ jobPositionSelected: value })
                }
                items={getJobPositions()}
                useNativeAndroidPickerStyle={false}
                textInputProps={{ underlineColor: "yellow" }}
                Icon={() => (
                  <Icon
                    name="chevron-small-down"
                    size={20}
                    color={theme.colors.primary}
                  />
                )}
                style={{
                  ...pickerSelectStyles,
                  iconContainer: {
                    top: 10,
                    right: 12,
                  },
                }}
              />
            </View>
            <Button
              gradient
              onPress={() => handleCreateUser()}
              style={{ marginTop: 20 }}
            >
              {fields.loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text bold white center>
                  Crear empleado
                </Text>
              )}
            </Button>
          </Block>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  buttonDate: {
    height: 42,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: theme.colors.gray2,
    flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
    marginTop: 5,
    marginBottom: 10,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  pickerContainer: {},
  hasErrors: {
    borderBottomColor: theme.colors.accent,
  },
});
