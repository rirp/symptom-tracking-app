import { makeRequest } from "./Network";
import { useSession } from "../context/context";
import CreateUserStore from "../stores/CreateUserStore";
import { useNavigation } from "@react-navigation/native";
import { CREATE_EMPLOYEE } from "../constants/api";

export default CreateUserAction = () => {
  const [fields, setFields] = CreateUserStore();
  const { sessionData, showErrorMessage, errorMessage } = useSession();
  const navigation = useNavigation();

  const getJobPositions = () => {
    return sessionData.jobPositions.map((a) => ({
      label: a.name,
      value: a.id.toString(),
    }));
  };

  const createEmployee = () => {
    return makeRequest(CREATE_EMPLOYEE, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        address: fields.address,
        birthDay: fields.birthDay,
        documentNumber: fields.docNumber,
        documentType: fields.docType.value,
        firstName: fields.firstName,
        gender: fields.gender,
        jobPositionId: fields.jobPositionSelected,
        lastName: fields.lastName,
        secondName: fields.secondName,
        surname: fields.surname,
        userType: "NORMAL",
      }),
    });
  };

  const validateEmployee = () => {
    const errors = [];

    setFields({ errors: [] });

    if (fields.docNumber == null || fields.firstName.length == 0) {
      errors.push("docNumber");
    }

    if (fields.firstName == null || fields.firstName.length == 0) {
      errors.push("firstName");
    }

    if (fields.lastName == null || fields.lastName.length == 0) {
      errors.push("lastName");
    }

    const currentDate = new Date().toISOString().split("T")[0];

    if (errors.length > 0) {
      setFields({ errors: errors });
    } else if (
      fields.jobPositionSelected == null ||
      fields.jobPositionSelected == 0
    ) {
      showErrorMessage("Debe seleccionar un cargo");
    } else if (fields.birthDay == currentDate) {
      showErrorMessage("Fecha de nacimiento debe ser diferente a la actual");
    } else {
      setFields({ errors: [], loading: true });
      createEmployee()
        .then(() => {
          setFields({ loading: false });
          navigation.navigate("Complete");
        })
        .catch((error) => {
          showErrorMessage(error.response.message);
          setFields({
            loading: false,
          });
        });
    }
  };
  return { validateEmployee, getJobPositions, createEmployee, createEmployee };
};
