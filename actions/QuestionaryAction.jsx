import { makeRequest } from "./Network";
import QuestionaryStore from "../stores/QuestionaryStore";
import { SAVE_ANSWERS } from "../constants/api";
import { useSession } from "../context/context";

function validateForm(questions) {
  var answers = [];

  for (let e of questions) {
    if (e.open && e.answerText != null) {
      answers.push({
        questionId: e.id,
        answerSelected: null,
        answerText: e.answerText,
      });
    } else {
      if (e.answerSelected != null && parseInt(e.answerSelected) > 0) {
        if (
          e.justification != undefined &&
          e.answerSelected == e.justification.answerIdToRequest &&
          e.justificationAsnwwer == null
        ) {
          console.log("hola1");
          return null;
        }
        console.log("hola");
        answers.push({
          questionId: e.id,
          answerSelected: e.answerSelected,
          answerText: null,
          justification: e.justificationAsnwwer,
        });
      }
    }
  }

  if (answers.length != questions.length) {
    answers = null;
  }

  return answers;
}

export default QuestionaryActions = () => {
  const [questionaryFields, setQuestionaryFields] = QuestionaryStore();
  const { showErrorMessage } = useSession();

  const updateQuestion = (question) => {
    if (question != undefined) {
      if (question.antecedent == true) {
        const result = questionaryFields.antecedents.map((item) => {
          if (item.id == question.id) {
            return question;
          }

          return item;
        });
        setQuestionaryFields({
          antecedents: result,
          followUp: questionaryFields.followUp,
        });
      } else {
        const result = questionaryFields.followUp.map((item) => {
          if (item.id == question.id) {
            return question;
          }
          return item;
        });
        setQuestionaryFields({
          followUp: result,
          antecedents: questionaryFields.antecedents,
        });
      }
    }
  };

  function saveAnswerInMemory(question, data) {
    const questionUpdated = {
      ...question,
      answerText: data.answerText,
      answerSelected: data.answerSelected,
      justificationAsnwwer: data.justificationAsnwwer,
    };

    updateQuestion(questionUpdated);
  }

  const handleQuestionary = () => {
    var answers = [];
    let aux = [];

    showErrorMessage(null);
    setQuestionaryFields({
      loading: true,
    });

    if (questionaryFields.requestAntecedent) {
      const antecedents = validateForm(questionaryFields.antecedents);

      if (antecedents == undefined) {
        return null;
      }

      answers = answers.concat(antecedents);
    }

    aux = validateForm(questionaryFields.followUp);

    return aux ? answers.concat(aux) : null;
  };

  const consumeSaveAnswers = (employeeId, answers) => {
    return makeRequest(SAVE_ANSWERS, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        employeeId: employeeId,
        answerList: answers,
      }),
    });
  };

  return {
    updateQuestion,
    handleQuestionary,
    saveAnswerInMemory,
    consumeSaveAnswers,
  };
};
