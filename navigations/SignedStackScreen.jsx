import React from "react";
import { Image, Platform } from "react-native";
import { theme } from "../constants";
import { createStackNavigator } from "@react-navigation/stack";
import { Questionary, Temperature, SearchUser, Complete } from "../screens";
import { HeaderTitle } from "../components";

const SignedStack = createStackNavigator();

export default function SignedStackScreen() {
  return (
    <SignedStack.Navigator
      screenOptions={{
        headerShown: true,
        headerTransparent: true,
        headerTitle: () => (
          <HeaderTitle textWithoutAcent={"WIP"} textWithAcent={"App"} />
        ),
        headerBackTitleVisible: false,
        headerBackImage: () => (
          <Image
            source={require("../assets/icons/back.png")}
            style={{ tintColor: theme.colors.primary }}
          />
        ),
        headerLeftContainerStyle: {
          alignItems: "center",
          marginLeft: theme.sizes.base * (Platform.OS == "ios" ? 2 : 1),
          paddingRight: theme.sizes.base,
        },
      }}
    >
      <SignedStack.Screen name="SearchUser" component={SearchUser} />
      <SignedStack.Screen name="Questionary" component={Questionary} />
      <SignedStack.Screen name="Temperature" component={Temperature} />
      <SignedStack.Screen name="Complete" component={Complete} />
    </SignedStack.Navigator>
  );
}
