import { useState, useEffect } from "react";
import { values } from "../constants";
import { documentTypes2 } from "../constants/values";

let listeners = [];

export let initialStore = {
  loading: false,
  errors: [],
  address: "",
  birthDay: new Date().toISOString().split("T")[0],
  docNumber: "",
  docType: values.documentTypes[0],
  firstName: "",
  gender: "M",
  jobPositionId: 0,
  lastName: "",
  secondName: "",
  surname: "",
  userType: "NORMAL",
  jobPositionSelected: 0,
};

let store = initialStore;

const setStore = (newStore) => {
  store = { ...store, ...newStore };
  listeners.forEach((listener) => {
    listener(store);
  });
};

const SearchUserStore = () => {
  const newListener = useState()[1];
  useEffect(() => {
    listeners.push(newListener);
    newListener(store);
    return () => {
      listeners = listeners.filter((listener) => listener !== newListener);
    };
  });
  return [store, setStore];
};

export default SearchUserStore;
