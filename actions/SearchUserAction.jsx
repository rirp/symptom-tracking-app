import { makeRequest } from "./Network";
import { useSession } from "../context/context";
import SearchUserStore from "../stores/SearchUserStore";
import { FIND_USER_BY_DOCUMENT } from "../constants/api";

export default function SearchUserAction() {
  const { showErrorMessage } = useSession();
  const [searchUserFields, setSearchUserFields] = SearchUserStore();

  const consumeFindUser = (docType, number) => {
    return makeRequest(FIND_USER_BY_DOCUMENT, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        documentType: docType,
        documentNumber: number,
      }),
    });
  };

  const findUser = () => {
    // reset values
    showErrorMessage(null);
    setSearchUserFields({
      userName: null,
      loading: false,
      employeeToSurvey: null,
    });

    consumeFindUser(searchUserFields.docType.value, searchUserFields.number)
      .then((response) => {
        const name = response.response.firstName.concat(
          " ",
          response.response.lastName
        );

        setSearchUserFields({
          userName: name,
          jobPosition: response.response.jobPosition.name,
          loading: false,
          employeeToSurvey: response.response.id,
          requestAntecedent: response.response.requestAntecedent,
        });
      })
      .catch((error) => {
        showErrorMessage(error.response.message);
        setSearchUserFields({
          loading: false,
        });
      });
  };

  return {
    findUser: findUser,
  };
}
