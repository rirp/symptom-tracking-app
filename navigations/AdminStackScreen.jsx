import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { HeaderTitle } from "../components";
import { Image, Platform } from "react-native";
import { theme } from "../constants";
import {
  Home,
  Questionary,
  Temperature,
  SearchUser,
  CreateUser,
  Complete,
  WebViewScreen,
} from "../screens";

const AdminStack = createStackNavigator();

export default function AdminStackScreen() {
  return (
    <AdminStack.Navigator
      initialRouteName={"Home"}
      screenOptions={{
        headerShown: true,
        headerTransparent: true,
        headerTitle: () => (
          <HeaderTitle textWithoutAcent={"WIP"} textWithAcent={"App"} />
        ),
        headerBackTitleVisible: false,
        headerBackImage: () => (
          <Image
            source={require("../assets/icons/back.png")}
            style={{ tintColor: theme.colors.primary }}
          />
        ),
        headerLeftContainerStyle: {
          alignItems: "center",
          marginLeft: theme.sizes.base * (Platform.OS == "ios" ? 2 : 1),
          paddingRight: theme.sizes.base,
        },
      }}
    >
      <AdminStack.Screen name="Home" component={Home} />
      <AdminStack.Screen
        name="WebView"
        component={WebViewScreen}
        options={{ headerTransparent: false }}
      />
      <AdminStack.Screen name="SearchUser" component={SearchUser} />
      <AdminStack.Screen name="Questionary" component={Questionary} />
      <AdminStack.Screen name="Temperature" component={Temperature} />
      <AdminStack.Screen name="CreateUser" component={CreateUser} />
      <AdminStack.Screen name="Complete" component={Complete} />
    </AdminStack.Navigator>
  );
}
