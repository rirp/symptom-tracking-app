import React from "react";

export const AppContext = React.createContext();

const defaultMsg = "Ha ocurrido un error inesperado";

export function AppProvider(props) {
  const [errorMessage, setErrorMessage] = React.useState({
    message: defaultMsg,
    show: false,
  });
  const [sessionData, setSessionData] = React.useState(null);

  const showErrorMessage = (errorMessage) => {
    const show = errorMessage != null;
    const msg = errorMessage == null ? defaultMsg : errorMessage;
    setErrorMessage({ message: msg, show: show });
    setSessionData(sessionData);
  };

  function reset(questions) {
    if (questions) {
      return questions.map((q) =>
        q.answerSelected != 0 ? { ...q, answerSelected: 0 } : q
      );
    }
  }

  const initSessionData = (
    userType,
    analyticsUrl,
    antecedents,
    followUp,
    jobPositions
  ) => {
    setSessionData({
      analyticsUrl: analyticsUrl,
      userType: userType,
      antecedents: antecedents,
      followUp: followUp,
      jobPositions: jobPositions,
    });
  };

  const updateQuestionary = (antecedents, followUp) => {
    setSessionData({
      ...sessionData,
      antecedents: antecedents,
      followUp: followUp,
    });
  };

  const session = React.useMemo(() => {
    return {
      errorMessage,
      sessionData,
      showErrorMessage,
      initSessionData,
      updateQuestionary,
    };
  });

  return <AppContext.Provider value={session} {...props} />;
}

export function useSession() {
  const context = React.useContext(AppContext);

  if (!context) {
    throw new Error("useSession must be into App provider");
  }
  return context;
}
