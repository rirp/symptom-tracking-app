import React, { useEffect } from "react";
import {
  StyleSheet,
  KeyboardAvoidingView,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Keyboard,
  Platform,
} from "react-native";
import pickerSelectStyles from "../constants/pickerSelectStyles";
import { Block, Text, Input, Button, Card } from "../components";
import SearchUserStore from "../stores/SearchUserStore";
import SearchUserAction from "../actions/SearchUserAction";
import { values, theme } from "../constants";
import RNPickerSelect from "react-native-picker-select";
import Icon from "react-native-vector-icons/Entypo";
import { SafeAreaView } from "react-native-safe-area-context";

export default function SearchUser({ navigation }) {
  const [searchUserFields, setSearchUserFields] = SearchUserStore();
  const actions = SearchUserAction();

  useEffect(() => {
    searchUserFields.userName = null;
    searchUserFields.number = null;
    searchUserFields.docType = values.documentTypes[0];
    searchUserFields.employeeToSurvey = null;
  }, []);

  function findUser() {
    Keyboard.dismiss();

    actions.findUser();
  }

  const renderUserCard = () => {
    return searchUserFields.userName != null ? (
      <Block column style={styles.userCard}>
        <Text h1>Usuario encontrado</Text>
        <TouchableOpacity style={{ height: 220 }}>
          <Card shadow>
            <Text medium height={20}>
              {searchUserFields.userName}
            </Text>
            <Text gray caption style={{ marginTop: 10 }}>
              {searchUserFields.jobPosition}
            </Text>
            <Button
              gradient
              onPress={() =>
                navigation.navigate("Questionary", {
                  employeeToSurvey: searchUserFields.employeeToSurvey,
                  requestAntecedent: searchUserFields.requestAntecedent,
                })
              }
            >
              <Block row center middle>
                <Icon name="list" size={30} color={theme.colors.white} />
                <Text bold white center style={{ marginLeft: 10 }}>
                  Realizar encuesta
                </Text>
              </Block>
            </Button>
            <Button
              gradient
              onPress={() =>
                navigation.navigate("Temperature", {
                  employeeToSurvey: searchUserFields.employeeToSurvey,
                })
              }
            >
              <Block row center middle>
                <Icon name="thermometer" size={30} color={theme.colors.white} />
                <Text bold white center style={{ marginLeft: 10 }}>
                  Tomar Temperatura
                </Text>
              </Block>
            </Button>
          </Card>
        </TouchableOpacity>
      </Block>
    ) : null;
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "space-between",
        backgroundColor: "white",
      }}
    >
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <Block padding={[80, theme.sizes.base * 2]} color={"white"}>
          <Text h2 center>
            Ingrese los datos del empleado
          </Text>
          <View style={styles.pickerContainer}>
            <RNPickerSelect
              placeholder={values.documentTypes[0]}
              onValueChange={(value, index) =>
                setSearchUserFields({
                  docType: values.documentTypes[index],
                })
              }
              items={values.documentTypes2}
              Icon={() => (
                <Icon
                  name="chevron-small-down"
                  size={24}
                  color={theme.colors.primary}
                />
              )}
              useNativeAndroidPickerStyle={false}
              textInputProps={{ underlineColor: "yellow" }}
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 10,
                  right: 12,
                },
              }}
            />
          </View>
          <Input
            label="Numero de documento"
            style={[styles.input]}
            defaultValue={searchUserFields.number}
            onChangeText={(text) => setSearchUserFields({ number: text })}
          />

          <Button gradient onPress={() => findUser()}>
            {searchUserFields.loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text bold white center>
                Buscar
              </Text>
            )}
          </Button>

          {renderUserCard()}
        </Block>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  input: {
    marginTop: 10,
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  card: {
    flex: 1,
  },
  userCard: {
    flex: 1,
    marginTop: 30,
    height: 100,
  },
  droidSafeArea: {
    flex: 1,
  },
  header: {
    marginTop: 45,
    marginBottom: 5,
    marginLeft: 20,
    marginRight: 20,
  },
  pickerContainer: { marginTop: 30 },
});
