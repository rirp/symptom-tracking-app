import React from "react";
import { Image, Dimensions } from "react-native";
import { Button, Block, Text } from "../components";
import { theme } from "../constants";

const { width, height } = Dimensions.get("window");

export default function Welcome({ navigation }) {
  function renderIllustrations() {
    return (
      <Image
        source={require("../assets/images/logo.png")}
        resizeMode="contain"
        style={{
          width: width - 60,
          height: height / 2,
          overflow: "visible",
        }}
      />
    );
  }

  return (
    <Block color="white">
      <Block center bottom flex={0.4}>
        <Text h1 center bold>
          WIP
          <Text h1 primary>
            {" "}
            App
          </Text>
        </Text>
        <Text h3 gray2 style={{ marginTop: theme.sizes.padding / 2 }}>
          Seguimiento de sintomas
        </Text>
      </Block>
      <Block center middle>
        {renderIllustrations()}
      </Block>
      <Block middle flex={0.5} margin={[0, theme.sizes.padding * 2]}>
        <Button gradient onPress={() => navigation.navigate("Login")}>
          <Text center semibold white>
            Iniciar sesión
          </Text>
        </Button>
      </Block>
    </Block>
  );
}
