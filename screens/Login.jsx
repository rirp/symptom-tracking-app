import React from "react";
import {
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  SafeAreaView,
  Platform,
} from "react-native";

import { Button, Block, Input, Text } from "../components";
import { theme } from "../constants";
import LoginStore from "../stores/LoginStore";
import LoginAction from "../actions/LoginAction";
import { useSession } from "../context/context";

const Login = () => {
  const { initSessionData } = useSession();
  const [loginFields, setloginFields] = LoginStore();
  const { login, validateError } = LoginAction();

  // Input error state
  const { errors } = loginFields;
  const hasErrors = (key) => (errors.includes(key) ? styles.hasErrors : null);

  const validateSuccess = (response) => {
    initSessionData(
      response.userType,
      response.analyticsUrl,
      response.antecedentQuestions,
      response.followUpQuestions,
      response.jobPositions
    );
    setloginFields({ errors: [], loading: false });
  };

  const handleLogin = () => {
    Keyboard.dismiss();
    setloginFields({ loading: true });

    login(loginFields.username, loginFields.password)
      .then((response) => validateSuccess(response.response))
      .catch((error) => validateError(error));
  };

  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <KeyboardAvoidingView
        style={{ flex: 1, justifyContent: "center" }}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <Block middle padding={[0, theme.sizes.base * 2]}>
          <Text h1 center bold>
            WIP
            <Text h1 primary>
              {" "}
              App
            </Text>
          </Text>
          <Input
            label="Usuario"
            error={hasErrors("username")}
            style={[styles.input, hasErrors("username")]}
            defaultValue={loginFields.username}
            onChangeText={(text) => setloginFields({ username: text })}
          />
          <Input
            secure
            label="Contraseña"
            error={hasErrors("password")}
            style={[styles.input, hasErrors("password")]}
            defaultValue={loginFields.password}
            onChangeText={(text) => setloginFields({ password: text })}
          />
          <Button gradient onPress={() => handleLogin()}>
            {loginFields.loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text bold white center>
                Iniciar sesión
              </Text>
            )}
          </Button>
        </Block>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent,
  },
  droidSafeArea: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});
