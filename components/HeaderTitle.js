import React, { Component } from "react";
import Text from "./Text";
import Block from "./Block";

export default class HeaderTitle extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { textWithoutAcent, textWithAcent } = this.props;

    return (
      <Block middle>
        <Text h2 center bold>
          {textWithoutAcent}
          <Text h2 primary>
            {" "}
            {textWithAcent}
          </Text>
        </Text>
      </Block>
    );
  }
}
