import React from "react";
import { WebView } from "react-native-webview";
import { useSession } from "../context/context";

export default function WebViewScreen() {
  const { sessionData } = useSession();
  return <WebView source={{ uri: sessionData.analyticsUrl }} />;
}
