import { useState, useEffect } from "react";

let listeners = [];

let store = {
  username: "",
  password: "",
  errors: [],
  loading: false,
};

const setStore = (newStore) => {
  store = { ...store, ...newStore };
  listeners.forEach((listener) => {
    listener(store);
  });
};

const LoginStore = () => {
  const newListener = useState()[1];
  useEffect(() => {
    listeners.push(newListener);
    newListener(store);
    return () => {
      listeners = listeners.filter((listener) => listener !== newListener);
    };
  });
  return [store, setStore];
};

export default LoginStore;
