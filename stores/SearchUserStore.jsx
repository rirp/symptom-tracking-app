import { useState, useEffect } from "react";
import { values } from "../constants";

let listeners = [];

let initialStore = {
  employeeToSurvey: 1,
  docType: values.documentTypes[0],
  number: "",
  loading: false,
  userName: "",
  jobPosition: "",
  requestAntecedent: false,
};

let store = initialStore;

const setStore = (newStore) => {
  store = { ...store, ...newStore };
  listeners.forEach((listener) => {
    listener(store);
  });
};

const SearchUserStore = () => {
  const newListener = useState()[1];
  useEffect(() => {
    listeners.push(newListener);
    newListener(store);
    return () => {
      listeners = listeners.filter((listener) => listener !== newListener);
    };
  });
  return [store, setStore];
};

export default SearchUserStore;
