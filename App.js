import React from "react";

import { Asset } from "expo-asset";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";

import Navigation from "./navigations";
import { AppProvider } from "./context/context";

const images = [
  require("./assets/icons/back.png"),
  require("./assets/images/logo.png"),
];

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  handleResourcesAsync = async () => {
    // we're caching all the images
    // for better performance on the app

    const cacheImages = images.map((image) => {
      return Asset.fromModule(image).downloadAsync();
    });

    return Promise.all(cacheImages);
  };

  render() {
    return (
      <SafeAreaProvider>
        <AppProvider>
          <Navigation />
        </AppProvider>
      </SafeAreaProvider>
    );
  }
}
