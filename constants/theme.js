import { Dimensions, PixelRatio, Platform } from "react-native";

const { width } = Dimensions.get("window");

const colors = {
  accent: "#F3534A",
  primary: "#0AC4BA",
  secondary: "#2BDA8E",
  tertiary: "#FFE358",
  black: "#323643",
  white: "#FFFFFF",
  gray: "#9DA3B4",
  gray2: "#C5CCD6",
};

const sizes = {
  // global sizes
  base: 16,
  font: normalize(14),
  radius: 6,
  padding: 25,

  // font sizes
  h1: normalize(26),
  h2: normalize(20),
  h3: normalize(18),
  title: normalize(18),
  header: normalize(16),
  body: normalize(14),
  caption: normalize(12),
  temperature: normalize(0.25 * width),
};

const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
};

function normalize(size) {
  const newSize = size;
  if (Platform.OS === "ios") {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}

export { colors, sizes, fonts };
