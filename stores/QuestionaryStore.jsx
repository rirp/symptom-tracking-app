import { useState, useEffect } from "react";

let listeners = [];
let initialStore = {
  employeeId: 0,
  loading: false,
  requestAntecedent: false,
};

let store = initialStore;

const setStore = (newStore) => {
  store = { ...store, ...newStore };
  listeners.forEach((listener) => {
    listener(store);
  });
};

const QuestionaryStore = () => {
  const newListener = useState()[1];
  useEffect(() => {
    listeners.push(newListener);
    newListener(store);
    return () => {
      listeners = listeners.filter((listener) => listener !== newListener);
    };
  });
  return [store, setStore];
};

export default QuestionaryStore;
