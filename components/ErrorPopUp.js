import React, { useEffect, useState } from "react";
import { StyleSheet, Modal } from "react-native";
import Block from "./Block";
import Text from "./Text";
import Button from "./Button";
import Icon from "react-native-vector-icons/AntDesign";
import { useSession } from "../context/context";

export default ErrorPopUp = (props) => {
  const { errorMessage, showErrorMessage } = useSession();

  const closeModal = () => {
    showErrorMessage(null);
  };

  return (
    <Block {...props}>
      <Modal
        visible={errorMessage.show}
        animationType={"fade"}
        onRequestClose={() => closeModal()}
      >
        <Block middle style={styles.modalContainer}>
          <Block middle>
            <Text h2 center bold>
              WIP
              <Text h2 primary>
                {" "}
                App
              </Text>
            </Text>
          </Block>
          <Block middle center flex={3} style={styles.contentText}>
            <Icon name={"warning"} color={"gold"} size={70} />
            <Text h1 semiBold center>
              {errorMessage.message}
            </Text>
          </Block>
          <Block flex={1}>
            <Button gradient style={styles.button} onPress={() => closeModal()}>
              <Text bold white center>
                Cerrar
              </Text>
            </Button>
          </Block>
        </Block>
      </Modal>
      <Block>{props.children}</Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    opacity: 0.7,
  },
  button: {
    marginHorizontal: 20,
  },
  contentText: {
    marginHorizontal: 40,
  },
});
