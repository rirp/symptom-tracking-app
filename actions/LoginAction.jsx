import { makeRequest } from "./Network";
import LoginStore from "../stores/LoginStore";
import { useSession } from "../context/context";
import { LOGIN } from "../constants/api";

export default LoginAction = () => {
  const [loginFields, setloginFields] = LoginStore();
  const { showErrorMessage } = useSession();

  const login = (username, password) => {
    return makeRequest(LOGIN, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
  };

  const validateError = (error) => {
    const errors = [];

    if (error.response.errorCode != null) {
      if (error.response.errorCode === 1) {
        errors.push("username");
      } else {
        errors.push("password");
      }
      setloginFields({ errors: errors, loading: false });
    } else {
      showErrorMessage(error.response.message);
      setloginFields({
        errors: [],
        loading: false,
      });
    }
  };

  return {
    login,
    validateError,
  };
};
