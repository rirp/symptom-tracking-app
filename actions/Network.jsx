export function makeRequest(url, options) {
  return new Promise((resolve, reject) => {
    fetch(url, options)
      .then(handleResponse)
      .then((response) => JSON.parse(response))
      .then((json) => resolve(json))
      .catch((error) => {
        try {
          reject(JSON.parse(error));
        } catch (e) {
          reject({
            response: {
              message: "Ha ocurrido un error en la comunicación",
              error: e,
            },
          });
        }
      });
  });
}

function handleResponse(response) {
  return response.json().then((json) => {
    // Modify response to include status ok, success, and status text
    let modifiedJson = {
      success: response.ok,
      status: response.status,
      statusText: response.statusText ? response.statusText : json.error || "",
      response: json,
    };
    // If request failed, reject and return modified json string as error
    if (modifiedJson.success == null || modifiedJson.status >= 400)
      return Promise.reject(JSON.stringify(modifiedJson));

    // If successful, continue by returning modified json string
    return JSON.stringify(modifiedJson);
  });
}
