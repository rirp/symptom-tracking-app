import { useState, useEffect } from "react";
import { values } from "../constants";

let listeners = [];

let initialStore = {
  employeeToSurvey: 0,
  loading: false,
  temp: 37,
  tempDec: 0,
  tempStr: "37.0",
};

let store = initialStore;

const setStore = (newStore) => {
  store = { ...store, ...newStore };
  listeners.forEach((listener) => {
    listener(store);
  });
};

const TemperatureStore = () => {
  const newListener = useState()[1];
  useEffect(() => {
    listeners.push(newListener);
    newListener(store);
    return () => {
      listeners = listeners.filter((listener) => listener !== newListener);
    };
  });
  return [store, setStore];
};

export default TemperatureStore;
