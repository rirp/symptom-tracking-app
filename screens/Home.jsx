import React from "react";
import {
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
} from "react-native";
import { Text, Block, Card, Badge } from "../components";
import { theme } from "../constants";
import { useSession } from "../context/context";
import Icon from "react-native-vector-icons/AntDesign";

const { width } = Dimensions.get("window");

const items = [
  {
    title: "Crear reporte",
    description: "Sintomas y antecedentes",
    icon: "barschart",
    color: theme.colors.primary,
    action: "SearchUser",
  },
  {
    title: "Crear usuario",
    description: "Crear usuario en tu empresa",
    icon: "adduser",
    color: theme.colors.secondary,
    action: "CreateUser",
  },
];

export default function Home({ navigation }) {
  const { sessionData } = useSession();

  const items =
    sessionData.analyticsUrl == null
      ? [
          {
            title: "Crear reporte",
            description: "Sintomas y antecedentes",
            icon: "barschart",
            color: theme.colors.primary,
            action: "SearchUser",
          },
          {
            title: "Crear usuario",
            description: "Crear usuario en tu empresa",
            icon: "adduser",
            color: theme.colors.secondary,
            action: "CreateUser",
          },
        ]
      : [
          {
            title: "Crear reporte",
            description: "Sintomas y antecedentes",
            icon: "barschart",
            color: theme.colors.primary,
            action: "SearchUser",
          },
          {
            title: "Crear usuario",
            description: "Crear usuario en tu empresa",
            icon: "adduser",
            color: theme.colors.secondary,
            action: "CreateUser",
          },
          {
            title: "Estadísticas",
            description: "información de la empresa",
            icon: "linechart",
            color: theme.colors.primary,
            action: "WebView",
          },
        ];

  const renderOption = (item) => {
    return (
      <TouchableOpacity
        key={item.title}
        onPress={() => navigation.push(item.action)}
      >
        <Card center middle shadow style={styles.category}>
          <Badge margin={[0, 0, 15]} size={50} color={item.color}>
            <Icon name={item.icon} size={20} color={theme.colors.white} />
          </Badge>
          <Text medium body height={20}>
            {item.title}
          </Text>
          <Text gray caption center>
            {item.description}
          </Text>
        </Card>
      </TouchableOpacity>
    );
  };

  const renderOpcions = () => {
    return (
      <Block row space="between" style={styles.categories}>
        {items.map((item) => renderOption(item))}
      </Block>
    );
  };

  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <Block flex={0.9}>{renderOpcions()}</Block>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    marginTop: 45,
    marginLeft: 10,
  },
  categories: {
    alignContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.padding,
    marginBottom: theme.sizes.base * 3.5,
  },
  droidSafeArea: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
  category: {
    // this should be dynamic based on screen width
    minWidth: (width - theme.sizes.padding * 2.5 - theme.sizes.base) / 2,
    maxWidth: (width - theme.sizes.padding * 2.5 - theme.sizes.base) / 2,
    maxHeight: (width - theme.sizes.padding * 2.5 - theme.sizes.base) / 2,
  },
});
