import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { useSession } from "../context/context";
import { ErrorPopUp, Block } from "../components";

import SignedStackScreen from "./SignedStackScreen";
import AdminStackScreen from "./AdminStackScreen";
import AuthStackScreen from "./AuthStackScreen";

const RootStack = createStackNavigator();

const RootStackScreen = ({ sessionData }) => {
  return (
    <RootStack.Navigator headerMode="none">
      {sessionData == null ? (
        <RootStack.Screen
          name="Login"
          component={AuthStackScreen}
          options={{
            animationEnabled: false,
          }}
        />
      ) : (
        <RootStack.Screen
          name="App"
          component={
            sessionData.userType == "ADMIN"
              ? AdminStackScreen
              : SignedStackScreen
          }
          options={{
            animationEnabled: false,
          }}
        />
      )}
    </RootStack.Navigator>
  );
};

export default function Navigation() {
  const { sessionData } = useSession();
  return (
    <Block>
      <ErrorPopUp>
        <NavigationContainer>
          <RootStackScreen sessionData={sessionData} />
        </NavigationContainer>
      </ErrorPopUp>
    </Block>
  );
}
