import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
  Dimensions,
  Platform,
} from "react-native";
import { Block, Text, Button } from "../components";
import { theme } from "../constants";
import TemperatureStore from "../stores/TemperatureStore";
import Icon from "react-native-vector-icons/AntDesign";
import { saveTemprature } from "../actions/TemperatureAction";
import { useSession } from "../context/context";

const { width } = Dimensions.get("window");

export default function Temperature({ navigation, route }) {
  const { showErrorMessage } = useSession();
  const [temperatureFields, setTemperatureFields] = TemperatureStore();

  React.useEffect(() => {
    temperatureFields.employeeToSurvey = route.params.employeeToSurvey;
  }, []);

  const increaseTemp = () => {
    if (temperatureFields.temp < 44) {
      const tempStr = `${temperatureFields.temp + 1}.${
        temperatureFields.tempDec
      }`;
      setTemperatureFields({
        temp: ++temperatureFields.temp,
        tempStr: tempStr,
      });
    }
  };

  const increaseTempDec = () => {
    if (temperatureFields.tempDec < 9) {
      const tempStr = `${temperatureFields.temp}.${
        temperatureFields.tempDec + 1
      }`;
      setTemperatureFields({
        tempDec: ++temperatureFields.tempDec,
        tempStr: tempStr,
      });
    }
  };

  const decreaseTemp = () => {
    if (temperatureFields.temp > 0) {
      const tempStr = `${temperatureFields.temp - 1}.${
        temperatureFields.tempDec
      }`;
      setTemperatureFields({
        temp: --temperatureFields.temp,
        tempStr: tempStr,
      });
    }
  };

  const decreaseTempDec = () => {
    if (temperatureFields.tempDec > 0) {
      const tempStr = `${temperatureFields.temp}.${
        temperatureFields.tempDec - 1
      }`;
      setTemperatureFields({
        tempDec: --temperatureFields.tempDec,
        tempStr: tempStr,
      });
    }
  };

  const handleTemperature = () => {
    setTemperatureFields({ loading: true });

    saveTemprature(
      temperatureFields.employeeToSurvey,
      temperatureFields.tempStr
    )
      .then((response) => {
        setTemperatureFields({
          loading: false,
        });
        navigation.navigate("Complete");
      })
      .catch((error) => {
        showErrorMessage(error.response.message);
        setTemperatureFields({
          loading: false,
        });
      });
  };

  const getRed = (percent) => {
    if (percent >= 36 && percent <= 37.5) {
      return 40;
    } else if (percent < 37.5) {
      return 40;
    } else if (percent > 37.5) {
      return 255;
    } else {
      return 144;
    }
  };

  const getGreen = (percent) => {
    if (percent >= 36 && percent <= 37.5) {
      return 180;
    } else if ((percent > 37.5) & (percent <= 38.5)) {
      return 220;
    } else if (percent > 38.5 || percent < 36.5) {
      return 114;
    } else {
      return 40;
    }
  };

  const getBlue = (percent) => {
    if (percent < 36.5) {
      return 220;
    } else {
      return 40;
    }
  };

  const getGreenToRed = (percent) => {
    const r = getRed(percent);
    const g = getGreen(percent);
    const b = getBlue(percent);
    return "rgb(" + r + "," + g + "," + b + ")";
  };

  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <KeyboardAvoidingView
        style={{ flex: 1, justifyContent: "center" }}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <Block middle padding={[0, theme.sizes.base * 2]}>
          <Block flex={1} center middle>
            <Text h1>Ingrese su temperatura</Text>
          </Block>
          <Block flex={2} center middle>
            <Text
              size={0.2 * width}
              color={getGreenToRed(parseFloat(temperatureFields.tempStr))}
            >
              {temperatureFields.temp}.{temperatureFields.tempDec} ºC
            </Text>
          </Block>
          <Block flex={2} row space="between" center>
            <Block column middle style={styles.fieldContainer}>
              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => increaseTemp()}
              >
                <Icon
                  name="pluscircle"
                  size={30}
                  color={theme.colors.primary}
                />
              </TouchableOpacity>
              <Block center middle>
                <Text center middle style={styles.inputText}>
                  {temperatureFields.temp}
                </Text>
              </Block>

              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => decreaseTemp()}
              >
                <Icon
                  name="minuscircle"
                  size={30}
                  color={theme.colors.primary}
                />
              </TouchableOpacity>
            </Block>

            <Block column middle style={styles.fieldContainer}>
              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => increaseTempDec()}
              >
                <Icon
                  name="pluscircle"
                  size={30}
                  color={theme.colors.primary}
                />
              </TouchableOpacity>
              <Block center middle style={styles.input}>
                <Text center middle style={styles.inputText}>
                  {temperatureFields.tempDec}
                </Text>
              </Block>
              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => decreaseTempDec()}
              >
                <Icon
                  name="minuscircle"
                  size={30}
                  color={theme.colors.primary}
                />
              </TouchableOpacity>
            </Block>
          </Block>

          <Button gradient onPress={() => handleTemperature()}>
            {temperatureFields.loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text bold white center>
                Ingresar registro
              </Text>
            )}
          </Button>
        </Block>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 10,
  },
  droidSafeArea: {
    backgroundColor: "white",
    flex: 1,
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
  inputText: {
    fontSize: 0.1 * width,
  },
  fieldContainer: {
    minWidth: (width - theme.sizes.padding * 3 - theme.sizes.base) / 2,
    maxWidth: (width - theme.sizes.padding * 3 - theme.sizes.base) / 2,
    maxHeight: (width - theme.sizes.padding * 3 - theme.sizes.base) / 2,
  },
});
